import PropTypes from 'prop-types';
import Modal from './Modal';
import ModalFooter from './ModalFooter';

const ModalText = ({ isOpen, onClose, title, content}) => {
    return (
        <Modal isOpen={isOpen} onClose={onClose}>
            <div>
                <h2>{title}</h2>
                <p>{content}</p>
                <ModalFooter
                    firstText="Cancel"
                    firstClick={onClose}
                />
            </div>
        </Modal>
    );
};
ModalText.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,

};
export default ModalText;
