import PropTypes from 'prop-types';
import Modal from './Modal';
import ModalFooter from './ModalFooter';

const ModalImage = ({ isOpen, onClose, title, content, imageUrl, confirmText }) => {
    return (
        <Modal isOpen={isOpen} onClose={onClose}>
            <div>
                <img src={imageUrl} />
                <h2>{title}</h2>
                <p>{content}</p>
                <ModalFooter
                    firstText="Cancel"
                    firstClick={onClose}
                    secondaryText={confirmText}
                />
            </div>
        </Modal>
    );
};
ModalImage.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    title: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    imageUrl: PropTypes.string.isRequired,
    confirmText: PropTypes.string.isRequired,
};
export default ModalImage;
