import PropTypes from 'prop-types';


const ModalBody = ({ children }) => {
    return <div>{children}</div>;
};
ModalBody.propTypes = {
    children: PropTypes.node
};
export default ModalBody;