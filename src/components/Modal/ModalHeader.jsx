import PropTypes from 'prop-types';

const ModalHeader = ({ children }) => {
    return <div>{children}</div>;
};
ModalHeader.propTypes = {
    children: PropTypes.node.isRequired
};
export default ModalHeader;