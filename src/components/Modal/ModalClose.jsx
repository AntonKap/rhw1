import PropTypes from 'prop-types';

const ModalClose = ({ onClick }) => {
    return (
        <button className="modal-close-button" onClick={onClick}>
            X
        </button>
    );
};
ModalClose.propTypes = {
    onClick: PropTypes.func.isRequired
};
export default ModalClose;