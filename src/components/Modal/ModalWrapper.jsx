import PropTypes from 'prop-types';
import '../../styles/modalWrapper.scss';

const ModalWrapper = ({ children }) => {
    return <div className="modal-wrapper">{children}</div>;
};
ModalWrapper.propTypes = {
    children: PropTypes.node.isRequired
};
export default ModalWrapper;