import PropTypes from 'prop-types';

const ModalFooter = ({ firstText, secondaryText, firstClick, secondaryClick }) => {
    return (
        <div className="modal-footer">
            {firstText && <button onClick={firstClick}>{firstText}</button>}
            {secondaryText && <button onClick={secondaryClick}>{secondaryText}</button>}
        </div>
    );
};
ModalFooter.propTypes = {
    firstText: PropTypes.string,
    secondaryText: PropTypes.string,
    firstClick: PropTypes.func,
    secondaryClick: PropTypes.func
};
export default ModalFooter;